import QtQuick 2.7
import QtQuick.Window 2.0
import QtQuick.Controls 2.3
import QtLocation 5.6
import QtPositioning 5.6
import my.example.boundary 1.0
import my.example.citymanager 1.0
import "."

Window {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Slovensko")

    signal nextTown()

    Boundary { id: boundary }
    CityManager {
        id: citymanager
        onCityChanged: {
            city.text = citymanager.city
            myMap.cityPosition = citymanager.coord
        }
    }

    Plugin {
        id: mapPlugin
        name: "osm"
    }

    Row {
        width: window.width
        height: window.height

        MyMap {
            id: myMap
            width: parent.width - controlPanel.width
            height: parent.height
            p1active: controlPanel.p1active
            p2active: controlPanel.p2active
            p3active: controlPanel.p3active
            p1color: controlPanel.p1color
            p2color: controlPanel.p2color
            p3color: controlPanel.p3color
        }

        ControlPanel {
            id: controlPanel
            width: 120
            height: parent.height
            winner: myMap.winner
            onShowTownPos: myMap.canShowTownPos = true
            onShowNextTown: { citymanager.generateNextTown(); myMap.clearMe() }
        }
    }

    Text {
        id: city
        color: 'lime'
        font.pointSize: 20
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 10
        Rectangle {
            anchors.fill: parent
            color: 'black'
            z:-1
        }
    }

    MapSlider {
        from: 10
        value: 20
        to: 50
        stepSize: 1

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 10
        anchors.leftMargin: 10

        onValueChanged: myMap.radius = value*1000
    }

    MapSlider {
        from: 7
        value: 7
        to: 12
        stepSize: 0.1

        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.bottomMargin: 10
        anchors.leftMargin: 10

        onValueChanged: myMap.zoomLevel = value
    }
}
