#ifndef CITYMANAGER_H
#define CITYMANAGER_H

#include <QObject>
#include <QMap>
#include <QGeoCoordinate>
#include <QTimer>

class CityManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString city READ city NOTIFY cityChanged)
    Q_PROPERTY(QGeoCoordinate coord READ coord)

public:
    explicit CityManager(QObject *parent = nullptr);

    QString city() const { return _currentTown; }
    QGeoCoordinate coord() const { return _citiesLocationMap.value(_currentTown); }
    Q_INVOKABLE void generateNextTown() { timerFired(); }

public slots:
    void nextTown();

signals:
    void cityChanged();

private slots:
    void timerFired();

private:
    QMap<QString, QGeoCoordinate> _citiesLocationMap;
    QTimer *_timer;
    QString _currentTown = "";
};

#endif // CITYMANAGER_H
