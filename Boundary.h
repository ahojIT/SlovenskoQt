#ifndef BOUNDARY_H
#define BOUNDARY_H

#include <QObject>
#include <QGeoPath>

class Boundary : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<double> longitudes READ longitudes CONSTANT)
    Q_PROPERTY(QList<double> latitudes READ latitudes CONSTANT)
    Q_PROPERTY(QGeoCoordinate center READ center CONSTANT)

public:
    explicit Boundary(QObject *parent = nullptr);
    QList<double> longitudes() { return _longitudes; }
    QList<double> latitudes() { return _latitudes; }
    QGeoCoordinate center() { return _path.center(); }

signals:

public slots:

private:
    QGeoPath _path = QGeoPath();
    QList<double> _longitudes = QList<double>();
    QList<double> _latitudes = QList<double>();
};

#endif // BOUNDARY_H
