import QtQuick 2.6
import QtQuick.Controls 2.1
import "common.js" as CommonJS

CheckBox {
    id: control
    text: qsTr("CheckBox")
    checked: false

    property alias color: innerRect.color
    property bool winner: false

    onWinnerChanged: {
        if (winner) {
            value.text = value.text+"⊙"
            winner = false
        }
    }

    indicator: Rectangle {
        implicitWidth: 26
        implicitHeight: 26
        x: control.leftPadding
        y: parent.height / 2 - height / 2
        radius: 3
        border.color: control.down ? "#17a81a" : "#21be2b"

        Rectangle {
            id: innerRect
            width: 14
            height: 14
            x: 6
            y: 6
            radius: 2
            color: control.down ? "transparent" : CommonJS.getRandomColor()
            visible: control.checked
        }
    }

    contentItem: Column {
        Text {
            text: control.text
            font: control.font
            opacity: checked ? 1.0 : 0.3
            color: control.down ? "#17a81a" : "#21be2b"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            leftPadding: control.indicator.width + control.spacing
        }
        Text {
            id: value
            text: ""
            font.pixelSize: 15
            opacity: checked ? 1.0 : 0.3
            color: 'red'
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            leftPadding: control.indicator.width + control.spacing
        }
    }
}

