#include "CityManager.h"
#include <QFile>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QGeoCoordinate>
#include <QRandomGenerator>

CityManager::CityManager(QObject *parent) : QObject(parent)
{
    QFile file(":/geojson/mesta"); // fila contains epsg4326 geojson format
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        qDebug() << "Can't open the boundary file" << file.fileName();

    // from GeoJson
    QByteArray data = file.readAll();
    QJsonDocument jsdoc = QJsonDocument::fromJson(data);
    QJsonArray cityArray = jsdoc.object()["features"].toArray();
    //QJsonArray coordinates = jsdoc.object()["features"].toArray().first().toObject()["geometry"].toObject()["coordinates"].toArray().first().toArray();

    for(int i=0; i<cityArray.size(); i++) {
        QJsonObject cityObject = cityArray.at(i).toObject();
        QString type = cityObject["properties"].toObject()["type"].toString();
        if(type == "town") {
            QString name = cityObject["properties"].toObject()["name"].toString();
            double longitude = cityObject["geometry"].toObject()["coordinates"].toArray().first().toDouble();
            double latitude = cityObject["geometry"].toObject()["coordinates"].toArray().last().toDouble();
            _citiesLocationMap.insert(name, QGeoCoordinate(latitude, longitude));
        }
    }

    _timer = new QTimer(this);
    _timer->start(100);
    connect(_timer, SIGNAL(timeout()), SLOT(timerFired()));
}

void CityManager::nextTown()
{
    timerFired();
}

void CityManager::timerFired()
{
    quint32 i = QRandomGenerator::global()->bounded(_citiesLocationMap.size()-1);
    _currentTown = _citiesLocationMap.keys().at(i);
    qDebug() << _currentTown << _citiesLocationMap.value(_currentTown);
    emit cityChanged();
    _timer->stop();
}
