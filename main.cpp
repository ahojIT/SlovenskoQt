#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include "Boundary.h"
#include "CityManager.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<Boundary>("my.example.boundary", 1, 0, "Boundary");
    qmlRegisterType<CityManager>("my.example.citymanager", 1, 0, "CityManager");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
