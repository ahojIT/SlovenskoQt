import QtQuick 2.6
import QtQuick.Controls 2.1

Slider {
    id: control

    opacity: 1

    //MouseArea {
    //    id: mouseArea
    //    hoverEnabled: true
    //    anchors.fill: parent
    //    onEntered: { console.log("XXX"); pAnim.running = true; enabled = false }
    //}

    background: Rectangle {
        x: control.leftPadding
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 200
        implicitHeight: 4
        width: control.availableWidth
        height: implicitHeight
        radius: 2
        color: "#bdbebf"

        Rectangle {
            width: control.visualPosition * parent.width
            height: parent.height
            color: "#21be2b"
            radius: 2
        }
    }

    handle: Rectangle {
        x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 26
        implicitHeight: 26
        radius: 13
        color: control.pressed ? "#f0f0f0" : "#f6f6f6"
        border.color: "#bdbebf"

        Text {
            id: km
            text: control.value
            font.bold: true

            anchors.centerIn: parent
        }
    }

    SequentialAnimation {
        id: pAnim
        PropertyAnimation {
            id: anim;
            target: control;
            property: "opacity"
            to: 1
            duration: 300
        }
        PropertyAnimation {
            id: delay;
            target: control;
            property: "opacity"
            to: 1
            duration: 5000
        }
        PropertyAnimation {
            id: hideAnim;
            target: control;
            property: "opacity"
            to: 0.1
            duration: 300
        }
        onStopped: mouseArea.enabled = true
    }
}
