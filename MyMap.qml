import QtQuick 2.7
import QtLocation 5.6
import QtPositioning 5.6
import "common.js" as CommonJS

Map {
    id: map

    property alias cityPosition: circle.center
    property alias p1active: player1.visible
    property alias p2active: player2.visible
    property alias p3active: player3.visible
    property alias p1color: player1.color
    property alias p2color: player2.color
    property alias p3color: player3.color
    property int radius: 20000
    property int winner: CommonJS.minIndex(p1active ? player1.center.distanceTo(circle.center) : radius,
                                           p2active ? player2.center.distanceTo(circle.center) : radius,
                                           p3active ? player3.center.distanceTo(circle.center) : radius,
                                           radius)
    property bool canShowTownPos: false

    plugin: mapPlugin
    center: boundary.center
    zoomLevel: 7
    copyrightsVisible: false
    color: 'blue'
    gesture.enabled: false

    // current town coordination
    Component.onCompleted: {
        map.addMapItem(circle)

        map.addMapItem(polygon)
        var x = []
        for(var i=0; i<boundary.longitudes.length; i++)
            x[i] = {latitude: boundary.latitudes[i], longitude: boundary.longitudes[i]}
        polygon.path = x;
    }

    function clearMe() {
        canShowTownPos = false
        player1.center.latitude = 48
        player1.center.longitude = 20
        player2.center.latitude = 48
        player2.center.longitude = 21
        player3.center.latitude = 48
        player3.center.longitude = 22
    }

    MapPolygon {
        id: mask
        visible: true
        color: 'black'
        path: [
            { latitude: -90, longitude: 0 },
            { latitude: 90, longitude: 0 },
            { latitude: 90, longitude: -179 },
            { latitude: -90, longitude: -179 }
        ]
    }
    MapPolygon {
        id: mask2
        visible: true
        color: 'black'
        path: [
            { latitude: -90, longitude: 0 },
            { latitude: 90, longitude: 0 },
            { latitude: 90, longitude: 180 },
            { latitude: -90, longitude: 180 }
        ]
    }

    // Slovakia border
    MapPolygon {
        id: polygon
        visible: true
        color: 'lightgreen'
    }

    MapCircle {
        id: circle
        radius: 200000.0
        color: 'transparent'
        border.width: 2
        border.color: 'red'
        visible: canShowTownPos

        onVisibleChanged: circleAnim.running = true

        PropertyAnimation {
            id: circleAnim;
            target: circle;
            property: "radius"
            to: canShowTownPos ? map.radius : 200000;
            duration: canShowTownPos ? 1000 : 0
        }
    }

    PlayerMark { id: player1; center { latitude: 48; longitude: 20 } }
    PlayerMark { id: player2; center { latitude: 48; longitude: 21 } }
    PlayerMark { id: player3; center { latitude: 48; longitude: 22 } }
}
