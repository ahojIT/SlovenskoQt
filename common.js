function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function getRandomColor() {
    do {
        var r = getRandomInt(0, 255);
        var g = getRandomInt(0, 255);
        var b = getRandomInt(0, 255);
    } while(r+g+b > 400); // darker color only

    return rgbToHex(r, g, b);
}

function minIndex(p1, p2, p3, max) {
    var min = Math.min(p1, p2, p3, max);
    //console.log(p1 + " " +p2 + " " +p3)
    return (min >= max ? 0 : p1 === min ? 1 : p2 === min ? 2 : p3 === min ? 3 : 0)
}
