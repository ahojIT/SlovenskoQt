#include "Boundary.h"
#include <QFile>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QGeoCoordinate>

Boundary::Boundary(QObject *parent) : QObject(parent)
{
    QFile file(":/geojson/hranice"); // fila contains epsg4326 geojson format
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        qDebug() << "Can't open the boundary file" << file.fileName();

    // from GeoJson
    QByteArray data = file.readAll();
    QJsonDocument jsdoc = QJsonDocument::fromJson(data);
    QJsonArray coordinates = jsdoc.object()["features"].toArray().first().toObject()["geometry"].toObject()["coordinates"].toArray().first().toArray();

    // to GeoPath
    for(QJsonValue coord: coordinates) {
        _path.addCoordinate(QGeoCoordinate(coord.toArray().last().toDouble(), coord.toArray().first().toDouble()));
        _longitudes.append(coord.toArray().first().toDouble());
        _latitudes.append(coord.toArray().last().toDouble());
    }
}
