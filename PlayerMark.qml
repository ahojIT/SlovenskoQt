import QtQuick 2.0
import QtLocation 5.6

MapCircle {
    id: p
    radius: 3000.0
    border.width: 1
    border.color: 'white'

    MouseArea {
        id: dragArea
        anchors.fill: parent
        drag.target: parent
    }
}
