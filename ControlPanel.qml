import QtQuick 2.7
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4

Frame {
    id: control

    property alias p1active: player1.checked
    property alias p2active: player2.checked
    property alias p3active: player3.checked
    property alias p1color: player1.color
    property alias p2color: player2.color
    property alias p3color: player3.color
    property int winner: 0

    signal showTownPos()
    signal showNextTown()

    background: Rectangle {
        color: "#111"
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 2
        PlayerCheckBox { id: player1; text: "Ocko" }
        PlayerCheckBox { id: player2; text: "Mamka" }
        PlayerCheckBox { id: player3; text: "Teo" }
        Rectangle { Layout.fillHeight: true }
        Button {
            id: buttonWinner
            text: "Ďalšie mesto"
            visible: false
            background: Rectangle {
                implicitWidth: 75
                implicitHeight: 30
                color: buttonWinner.down ? "#17a81a" : "#21be2b"
                border.color: "#26282a"
                border.width: 1
                radius: 8
            }
            onClicked: {
                visible = false
                buttonShow.visible = true
                control.showNextTown()
            }
        }
        Button {
            id: buttonShow
            text: "Ukáž"
            visible: player1.checked | player2.checked | player3.checked
            background: Rectangle {
                implicitWidth: 75
                implicitHeight: 30
                color: buttonShow.down ? "#17a81a" : "#21be2b"
                border.color: "#26282a"
                border.width: 1
                radius: 8
            }
            onClicked: {
                visible = false
                buttonWinner.visible = true
                control.showTownPos()
                //console.log("Winner je " + winner)
                if(winner == 1)
                    player1.winner = true;
                else if(winner == 2)
                    player2.winner = true;
                else if(winner == 3)
                    player3.winner = true;
            }
        }
    }

}
